package com.example.retrofitexample;


// See https://api.github.com/users/octocat for more information
public class User {
	String login;
	String id;
	String avatar_url;
	
	public String getLogin() {
		return login;
	}
	public String getId() {
		return id;
	}
	public String getAvatar_url() {
		return avatar_url;
	}
}
