package com.example.retrofitexample;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

public class MainActivity extends ActionBarActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// ////////////////////////////////////////////
		// ////////////////////////////////////////////
		// ////////////////////////////////////////////
		//Initialize RestAdapter to the target url
		RestAdapter restAdapter = new RestAdapter.Builder().setEndpoint(
				"https://api.github.com").build();
		
		//Put GitHubService inside the RestAdapter to use function of GitHubService
		GitHubService service = restAdapter.create(GitHubService.class);
		
		service.getUserInfo("octocat", new Callback<User>(){

			@Override
			public void failure(RetrofitError arg0) {
				// TODO Auto-generated method stub
				Toast.makeText(getApplicationContext(), "Fail",
						Toast.LENGTH_LONG).show();
			}

			@Override
			public void success(User user, Response arg1) {
				String login = user.getLogin();
				String id = user.getId();
				String avatar_url = user.getAvatar_url();
				
				Toast.makeText(getApplicationContext(), login + id + avatar_url,
						Toast.LENGTH_LONG).show();
			}});
		// ////////////////////////////////////////////
		// ////////////////////////////////////////////
		// ////////////////////////////////////////////
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
