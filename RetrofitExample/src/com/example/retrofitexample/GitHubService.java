package com.example.retrofitexample;

import java.util.List;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;

public interface GitHubService {
	  @GET("/users/{user}")
	  void getUserInfo(@Path("user") String user, Callback<User> cb);
	}